#!/usr/bin/env python3
from wikstraktor import Wikstraktor, Pronunciation, Sense, SubSense, Definition

from parsers.en_constants import string_values
import re

debugEty = 0

class En_en_straktor(Wikstraktor):
	def __init__(self):
		super().__init__()
		self.wiki_language = "en"
		self.entry_language = "en"
		self.constants = string_values
		self.site = self.pwb.Site(f'wiktionary:en')

	def process_audio_accent(self, audio, file_name = False):
		if file_name: #on traite un nom de fichier
			match = re.search(r'(e|E)(n|N)-(\w\w)-(.*)\.ogg', audio)
			if match:
				res = match.group(3).upper()
			else:
				res = None
		else:
			res = re.sub(r'((a|A)udio ?\(?)|\)?','',audio)
			if res == "":
				res = None
		return res

	def process_pronunciation(self, proContent):
		# TODO: ne marche que pour les listes à 2 niveaux, voir water pour 3 niveaux
		l = proContent.get_lists()[0]
		i = 0
		pronunciations = []
		while i < len(l.fullitems):
			p = Pronunciation()
			templates = self.wtp.parse(l.fullitems[i]).templates
			acc = None
			for j, t in enumerate(templates):
				if (t.normal_name() == self.constants['t_acc'] and templates[j+1].normal_name()!= self.constants['t_acc']):
					for a in t.arguments:
						p.set_accent(a.value)
				elif t.normal_name() == self.constants['t_ipa']:
					p.set_transcription(t.arguments[1].value)
				elif t.normal_name() == self.constants['t_snd']:
					if len(t.arguments) > 2:
						acc = self.process_audio_accent(t.arguments[2].value)
					if acc == None:
						acc = self.process_audio_accent(t.arguments[1].value, True)
					p.add_sound(self.get_file_url(t.arguments[1].value), acc)
			if p.ipa != None or p.has_accents() or p.has_sounds():
				pronunciations.append(p)
			else:
				self.log.add_log("En_en_straktor.process_pronunciation", f"“{l.fullitems[i]}” processed as empty → {p}")
			i += 1
		return pronunciations

	def process_etymology(self, etyContent):
		global debugEty
		debugEty += 1
		return "Etymology" + str(debugEty)

	def parse_template_1(self, templates):
		the_def = None
		for t in templates:
			if t.normal_name() == "1":
				the_def = Definition(self.entry_language, f"Other wording of “{t.arguments[0].value}”")
				break
		return the_def

	def parse_labels(self, a_sense, templates):
		key = "labels"
		desc = "language"
		num = 0
		for t in templates:
			if t.normal_name() in self.constants['t_lbl']:
				while a_sense.metadata_exists(f"{key}_{num}_{desc}"):
					num+=1
				a_sense.add_metadata(f"{key}_{num}_{desc}", t.arguments[0].value)
				complete_previous = False
				for a in t.arguments[1:]:
					if a.value == "_":
						complete_previous = True
					elif a.value == "and":
						pass
					elif a.value in self.constants['regions'].keys():
						a_sense.add_region(self.constants['regions'][a.value])
					elif complete_previous:
						a_sense.extend_metadata(f"{key}_{num}", a.value, " ")
						complete_previous = False
					else:
						a_sense.add_to_metadata(f"{key}_{num}", a.value)

	def parse_definition(self, parsed_def):
		if not isinstance(parsed_def, self.wtp.WikiText):
			parsed_def = self.wtp.parse(parsed_def)
		def_text = parsed_def.plain_text().strip()
		templates = parsed_def.templates
		the_def = self.parse_template_1(templates)
		self.try_inflected_forms(templates)
		if the_def == None:
			the_def = self.parse_alt_spell(templates)
		if the_def == None:
			the_def = Definition(self.entry_language, def_text)
		if the_def == None:
			raise ValueError(f"En_en_straktor.parse_definition with empty definition\n\t{def_string}")
		return the_def

	def get_sense_metadata(self, sense, parsed_def):
		if not isinstance(parsed_def, self.wtp.WikiText):
			parsed_def = self.wtp.parse(parsed_def)
		templates = parsed_def.templates
		self.parse_labels(sense, templates)


	def process_POS(self,parsedwikitext):
		pos = None
		if parsedwikitext in self.constants['POS'].keys():
			pos = self.constants['POS'][parsedwikitext]
		return pos

	def check_see_also(self, parsedwikitext):
		templates = parsedwikitext.templates
		for t in templates:
			if t.normal_name() in self.constants['t_sa']:
				for p in t.arguments:
					self.add_redirect(p.value)

	def fetch(self, graphy, follow_redirections):
		res = super().fetch(graphy.lower(), follow_redirections)
		if res == 0:
			res = super().fetch(graphy, follow_redirections)
		return res

if __name__ == "__main__":
	ensk = En_en_straktor()
	print(ensk.fetch("test"), "entries added")
