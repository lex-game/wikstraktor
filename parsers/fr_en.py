#!/usr/bin/env python3
from wikstraktor import Wikstraktor, Pronunciation, Sense, get_list_string_level

from parsers.fr_constants import string_values

debugEty = 0

class Fr_en_straktor(Wikstraktor):
	def __init__(self):
		super().__init__()
		self.wiki_language = "fr"
		self.entry_language = "en"
		self.constants = string_values
		self.site = self.pwb.Site(f'wiktionary:fr')

	def process_pronunciation(self, proContent):
		# TODO: ne marche que pour les listes à 2 niveaux, voir water pour 3 niveaux
		l = proContent.get_lists()[0]
		i = 0
		pronunciations = []
		previous_level = None
		for item in l.fullitems:
			current_level = get_list_string_level(item)
			if previous_level == None or current_level <= previous_level:
				p = Pronunciation()
				pronunciations.append(p) #objects are pointers
			templates = self.wtp.parse(item).templates
			for j, t in enumerate(templates):
				if t.normal_name() == self.constants['t_ipa']:
					p.set_transcription(t.arguments[0].value)
				elif t.normal_name() == self.constants['t_snd']:
					if t.has_arg("audio"):
						f = t.get_arg("audio").value
					else:
						f = t.arguments[-1].value
					p.add_sound(self.get_file_url(f))
					if len(self.wtp.parse(t.get_arg("1").value).templates) != 1:#Royaume-Uni (Londres)
						p.set_accent(t.get_arg("1").value)
					else:#{{UK|nocat=1}}
						p.set_accent(self.wtp.parse(t.get_arg("1").value).templates[0].normal_name())
					if t.get_arg("2") != None:
						p.set_transcription(t.get_arg("2").value)
				elif t.has_arg("nocat") and t.normal_name() in self.constants['regions'].keys():
					p.set_accent(self.constants['regions'][t.normal_name()])
			i += 1
		return pronunciations

	def process_etymology(self, etyContent):
		global debugEty
		debugEty += 1
		return "Etymology" + str(debugEty)

	def process_POS(self,parsedwikitext):
		pos = None
		ik = 0
		values =  list(self.constants['POS'].values())
		while pos == None and ik < len(self.constants['POS'].keys()):
			if parsedwikitext in values[ik]:
				keys = list(self.constants['POS'].keys())
				pos = keys[ik]
			ik += 1
		return pos

	def check_see_also(self, parsedwikitext):
		templates = parsedwikitext.templates
		for t in templates:
			if t.normal_name() in self.constants['t_sa']:
				for p in t.arguments:
					self.add_redirect(p.value)
			elif any(s+"/" in t.normal_name() for s in self.constants['t_sa']):
				self.check_see_also(self.wtp.parse(self.pwb.Page(self.site, "Modèle:"+t.normal_name())))

if __name__ == "__main__":
	ensk = Fr_en_straktor()
	print(ensk.fetch("test"), "entries added")
