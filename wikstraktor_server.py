#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask_cors import CORS #to allow cross-origin requests
from wikstraktor import Wikstraktor
import wikstraktor_server_config as config

app = Flask(__name__)
CORS(app)

@app.route('/', methods=['GET'])
def index():
    c = request.remote_addr
    response = f"<p>Server is running, your ip is {c}</p>"
    return Response(response, 200)

@app.route('/search/<word>', methods=['GET'])
def default_search(word):
    return search(config.wiktlang, config.wordlang, word)

@app.route('/search/<wiktlang>/<wordlang>/<word>', methods=['GET'])
def search(wiktlang, wordlang, word):
    w = Wikstraktor.get_instance(wiktlang, wordlang)
    if w.fetch(word) > 0:
        resp = w.__str__()
        status = 200
        mimetype='application/json'
    else:
        resp = f"""<!doctype html>
<html>
    <head>
        <title>Error</title>
    </head>
    <body>
        <h1>{word}</h1>
        <p>{word} is unknown in “{wordlang}” in {wiktlang}.wiktionary.org.</p>
    </body>
</html>"""
        status = 404
        mimetype='text/html'
    return Response(resp, status=status, mimetype=mimetype)

if __name__ == "__main__":
    app.run(host=config.host, port=config.port, debug=config.debugging)
