Wikstraktor is a very lightweight program meant to give access to wiktionary data to programs. It is not complete nor fullproof but it can be integrated fast to your program and does not require to preparse dumps or anything. If you want some more exhaustive, reliable, wiktionary extractor, you should look at [Wiktextract](https://github.com/tatuylonen/wiktextract) or its [live query counterpart](https://gitlab.liris.cnrs.fr/lex-game/live-query-wiktextract).

    Copyright (C) 2024  Lex:gaMe project (LIRIS/INSA/ASLAN)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

The [Lex:gaMe](https://lexgame.lezinter.net) project is a research project of [INSA](https://insa-lyon.fr)/[LIRIS](https://liris.cnrs.fr) and [ICAR](https://icar.cnrs.fr/)/Université Lyon 2. The authors are grateful to the [ASLAN project](https://aslan.universite-lyon.fr/projet-lex-game-233220.kjsp) (ANR-10-LABX-0081) of the Université de Lyon, for its financial support within the French program "Investments for the Future" operated by the National Research Agency (ANR).