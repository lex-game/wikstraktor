#!/usr/bin/env python3
import sqlite3

class Wikstraklog:
    table_name = "Wikstraklog"
    def __init__(self, wikstraktor_version, word_language, wiki_language, file="wikstraktor.sqlite"):
        self.__connector__ = sqlite3.connect(file)
        self.__cursor__ = self.__connector__.cursor()
        if self.__execute__(f"SELECT name FROM sqlite_master WHERE type='table' AND name='{Wikstraklog.table_name}';").fetchone() is None :
            self.__execute__(f"""CREATE TABLE "{Wikstraklog.table_name}" (
	"Date"	DATETIME	DEFAULT CURRENT_TIMESTAMP,
	"Wikstraktor_version"	TEXT NOT NULL,
	"Word_language"	TEXT NOT NULL,
	"Wiki_language"	TEXT NOT NULL,
	"Word_form"	TEXT NOT NULL,
	"Wiki_permanent_id"	INTEGER NOT NULL,
	"Caller_method"	TEXT,
	"Content"	TEXT
);""")
        self.wx_v = wikstraktor_version
        self.w_l = word_language
        self.wk_l = wiki_language
        self.cur_w = None
        self.cur_pid = -1

    def set_context(self, word, permanentId):
        self.cur_w = word
        self.cur_pid = permanentId

    def __execute__(self, query, params = None):
        if params == None:
            res = self.__cursor__.execute(query)
        else:
            res = self.__cursor__.execute(query, params)
        self.__connector__.commit()
        return res

    def add_log(self, caller, content, word=None, permanentId=None):
        if word == None:
            word = self.cur_w
        if permanentId == None:
            permanentId = self.cur_pid
        res = self.__execute__(f"INSERT INTO `{Wikstraklog.table_name}` (`Wikstraktor_version`, `Word_language`, `Wiki_language`, `Word_form`, `Wiki_permanent_id`, `Caller_method`, `Content`) VALUES (?, ?, ?, ?, ?, ?, ?)", (self.wx_v, self.w_l, self.wk_l, word, permanentId, caller, str(content)))
        return res

if __name__ == "__main__":
    import git
    log = Wikstraklog(git.Repo(search_parent_directories=True).head.object.hexsha, "en", "fr")
    log.set_context("blue", 123456789)
    log.add_log("exampleMethod", "no relevant content")
